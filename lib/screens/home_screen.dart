import 'package:faipa_app/screens/PageOne.dart';
import 'package:faipa_app/screens/PageThree.dart';
import 'package:faipa_app/screens/PageTwo.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int currentIndex = 0;
  List pages = [PageOne(), PageTwo(), PageThree()];

  @override
  Widget build(BuildContext context) {

    Widget appBar = AppBar(
      title: Text(
        'โปรแกรมทดสอบ',
      ),
      actions: <Widget>[
        IconButton(
            icon: Icon(Icons.home),
            onPressed: () => Navigator.of(context).pushNamed('/photo')),
        IconButton(
            icon: Icon(Icons.account_circle),
            onPressed: () => Navigator.of(context).pushNamed('/add')),
      ],
    );



    Widget bottomNavBar = BottomNavigationBar(
        currentIndex: currentIndex,
        onTap: (int index) {
          setState(() {
            currentIndex = index;
          });
        },
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.map),
              title: Text(
                'แผนที่',
              )),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_circle),
              title: Text(
                'ข้อมูลส่วนตัว',
              )),
          BottomNavigationBarItem(
              icon: Icon(Icons.settings),
              title: Text(
                'ตั้งค่า',
              )),
        ]);


    return Scaffold(
      appBar: appBar,
      body: pages[currentIndex],
      bottomNavigationBar: bottomNavBar,
    );
  }
}
