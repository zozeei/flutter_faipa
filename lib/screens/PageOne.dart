import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:async';
class PageOne extends StatefulWidget {
  @override
  _PageOneState createState() => _PageOneState();
}

class _PageOneState extends State<PageOne> {
  Completer<GoogleMapController> _controller = Completer();
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(13.102443, 100.927880),
    zoom: 14.4746,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          GoogleMap(
            mapType: MapType.hybrid,
            initialCameraPosition: _kGooglePlex,
            onMapCreated: (GoogleMapController controller) {
              _controller.complete(controller);
            },
            markers: Set<Marker>.of(markers.values),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 30, right: 10),
            child: Align(
              alignment: Alignment.topRight,
              child: Column(
                children: <Widget>[
                  Container(
                    child: IconButton(
                      icon: Icon(
                        Icons.home,
                        color: Colors.green,
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: Colors.white),
                    height: 40,
                    width: 40,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    child: IconButton(
                      icon: Icon(
                        Icons.location_searching,
                        color: Colors.green,
                      ),
                      onPressed: () {
//                        gotoCurrentPosition();
//                        _add();
                      },
                    ),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: Colors.white),
                    height: 40,
                    width: 40,
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
